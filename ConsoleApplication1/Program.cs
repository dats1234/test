﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_team14
{
    class Program
    {
        static void Main(string[] args)
        {
            var repeat = true;
            while (repeat == true)
            {
                int menuchoice = 0;
                while (menuchoice != 5)
                {
                    Console.Clear();
                    Console.WriteLine("MENU");
                    Console.WriteLine("Please enter the number that you want to do:");
                    Console.WriteLine("1. Date Calculator");
                    Console.WriteLine("2. Grade Average");
                    Console.WriteLine("3. Generate a Random Number");
                    Console.WriteLine("4. Rate Favourite Food");
                    Console.WriteLine("5. Exit");
                    var menu = Console.ReadLine();
                    var a = 0;
                    bool compare = int.TryParse(menu, out a);
                    if (compare == true)
                    {
                        var choice = Convert.ToInt32(menu);
                        menuchoice = choice;

                        switch (menuchoice)
                        {
                            case 1:
                                Console.Clear();
                                Console.WriteLine("Hello Can I please get your age so I can work out how old you are please type you date of birth in dd/mm/yyyy please");
                                var birthday = Console.ReadLine();
                                Console.WriteLine($"you are {GetAge(birthday)} years old, press enter to get back to main menu");
                                Console.ReadLine();
                                Console.Clear();
                                break;
                            case 2:
                                Console.Clear();
                                Methodthreeone();
                                menuchoice = 0;
                                break;
                            case 3:
                                Console.Clear();
                                guessingGame();
                                menuchoice = 0;
                                break;
                            case 4:
                                Console.Clear();
                                FoodRank();
                                break;
                            case 5:
                                Console.Clear();
                                Console.ReadLine();
                                break;
                            default:
                                Console.WriteLine("Sorry, invalid selection");
                                break;
                        }
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("it needs to be a number from 1 to 5");
                        repeat = true;
                        break;
                    }
                }
            }
        }
        public static string GetAge(string birthDate)
        {
            var date = Convert.ToDateTime(birthDate);
            var todayDate = DateTime.Now;
            var todayEnd = todayDate.Date;
            var dateEnd = date.Date;
            var dayResult = todayEnd - dateEnd;
            var dayString = Convert.ToString(dayResult);
            string dayArray = dayString;
            string[] result = dayArray.Split(':');
            var convertEnd = Convert.ToDouble(result[0]);
            var final = Math.Round(convertEnd / 365);
            var endResult = Convert.ToString(final);
            return endResult;
        }
        public static void Methodthreeone()
        {
            Console.WriteLine("Are you doing DAC 5 or DAC 6? Please enter 5 for 5 and 6 for 6");
            var level = int.Parse((Console.ReadLine()));
            Console.WriteLine("What's your student id number?");
            var studentId = Console.ReadLine();
            var dacMarks = new Dictionary<string, Tuple<double, string>>();
            Console.Clear();
            {
                {

                    switch (level)
                    {
                        case 5:
                            Console.WriteLine("Please enter your course paper code and then marks for your 4 papers");
                            var dacFiveNumberOfCourses = 4;
                            var dacFiveMarks = new Dictionary<string, Tuple<double, string>>();
                            for (int i = 0; i < dacFiveNumberOfCourses; i++)
                            {
                                dacFiveMarks.Add(Console.ReadLine(), Methodthreethree());
                            }
                            dacMarks = dacFiveMarks;
                            int methodthreemenufive = 0;
                            while (methodthreemenufive != 4)
                            {
                                Console.Clear();
                                Console.WriteLine("Method Two Menu");
                                Console.WriteLine("Please Press 1 to see your papers and marks");
                                Console.WriteLine("Please Press 2 to see the average of your papers");
                                Console.WriteLine("Please Press 3 to see which papers you have an A+ score");
                                Console.WriteLine("Please Press 4 to return to the main menu");

                                methodthreemenufive = int.Parse(Console.ReadLine());
                                switch (methodthreemenufive)
                                {
                                    case 1:
                                        Console.Clear();
                                        foreach (KeyValuePair<string, Tuple<double, string>> entry in dacMarks)
                                        {
                                            Console.WriteLine("Course = {0}, Mark Percentage = {1}, Letter Grade = {2}", entry.Key, entry.Value.Item1, entry.Value.Item2);
                                        }
                                        Console.WriteLine("Please enter any key to return to Task 2 Menu");
                                        Console.ReadKey();
                                        Console.Clear();
                                        methodthreemenufive = 0;
                                        break;
                                    case 2:
                                        Console.Clear();
                                        Methodthreefour(dacMarks);
                                        Console.WriteLine("Please enter any key to return to Task 2 Menu");
                                        Console.ReadKey();
                                        Console.Clear();
                                        methodthreemenufive = 0;
                                        break;
                                    case 3:
                                        Console.Clear();
                                        Methodthreefive(dacMarks);
                                        Console.WriteLine("Please enter any key to return to Task 2 Menu");
                                        Console.ReadKey();
                                        Console.Clear();
                                        methodthreemenufive = 0;
                                        break;
                                    case 4:
                                        Console.Clear();
                                        break;
                                    default:
                                        Console.Clear();
                                        Console.WriteLine("Incorrect selection. Please enter a number 1-4");
                                        Console.WriteLine("Please enter any key to return to Task 2 Menu");
                                        Console.ReadKey();
                                        Console.Clear();
                                        methodthreemenufive = 0;
                                        break;
                                }
                            }
                            break;

                        case 6:
                            Console.WriteLine("Please enter the course paper code and then  marks for your 3 papers");
                            var dacSixNumberOfCourses = 3;
                            var dacSixMarks = new Dictionary<string, Tuple<double, string>>();
                            for (int i = 0; i < dacSixNumberOfCourses; i++)
                            {
                                dacSixMarks.Add(Console.ReadLine(), Methodthreethree());
                            }
                            Console.Clear();
                            dacMarks = dacSixMarks;
                            int methodthreemenusix = 0;
                            while (methodthreemenusix != 4)
                            {
                                Console.Clear();
                                Console.WriteLine("Method Two Menu");
                                Console.WriteLine("Please Press 1 to see your papers and marks");
                                Console.WriteLine("Please Press 2 to see the average of your papers");
                                Console.WriteLine("Please Press 3 to see which papers you have an A+ score");
                                Console.WriteLine("Please Press 4 to return to the main menu");

                                methodthreemenusix = int.Parse(Console.ReadLine());
                                switch (methodthreemenusix)
                                {
                                    case 1:
                                        Console.Clear();
                                        foreach (KeyValuePair<string, Tuple<double, string>> entry in dacMarks)
                                        {
                                            Console.WriteLine("Course = {0}, Mark Percentage = {1}, Letter Grade = {2}", entry.Key, entry.Value.Item1, entry.Value.Item2);
                                        }
                                        Console.WriteLine("Please press any key to return to Task 2 Menu");
                                        Console.ReadKey();
                                        Console.Clear();
                                        methodthreemenusix = 0;
                                        break;
                                    case 2:
                                        Console.Clear();
                                        Methodthreefour(dacMarks);
                                        Console.WriteLine("Please enter any key to return to Task 2 Menu");
                                        Console.ReadKey();
                                        Console.Clear();
                                        methodthreemenusix = 0;
                                        break;
                                    case 3:
                                        Console.Clear();
                                        Methodthreefive(dacMarks);
                                        Console.WriteLine("Please enter any key to return to Task 2 Menu");
                                        Console.ReadKey();
                                        Console.Clear();
                                        methodthreemenusix = 0;
                                        break;
                                    case 4:
                                        Console.Clear();
                                        break;
                                    default:
                                        Console.Clear();
                                        Console.WriteLine("Incorrect selection. Please enter a number 1-4");
                                        Console.WriteLine("Please enter any key to return to Task 2 Menu");
                                        Console.ReadKey();
                                        Console.Clear();
                                        methodthreemenusix = 0;
                                        break;
                                }
                            }
                            break;

                    }

                }

            }
        }
        public static Tuple<double, string> Methodthreethree()
        {
            var percentageScore = double.Parse(Console.ReadLine());
            string letterGradeScore;
            if (percentageScore >= 90)
            {
                letterGradeScore = "A+";
            }
            else if (percentageScore >= 85)
            {
                letterGradeScore = "A";
            }
            else if (percentageScore >= 80)
            {
                letterGradeScore = "A-";
            }
            else if (percentageScore >= 75)
            {
                letterGradeScore = "B+";
            }
            else if (percentageScore >= 70)
            {
                letterGradeScore = "B";
            }
            else if (percentageScore >= 65)
            {
                letterGradeScore = "B-";
            }
            else if (percentageScore >= 60)
            {
                letterGradeScore = "C+";
            }
            else if (percentageScore >= 55)
            {
                letterGradeScore = "C";
            }
            else if (percentageScore >= 50)
            {
                letterGradeScore = "C-";
            }
            else if (percentageScore >= 40)
            {
                letterGradeScore = "D";
            }
            else
            {
                letterGradeScore = "E";
            }
            var Tuple = new Tuple<double, string>(percentageScore, letterGradeScore);
            return Tuple;
        }
        public static void Methodthreefour(Dictionary<string, Tuple<double, string>> dacMarks)
        {
            var data = dacMarks;
            double average = 0;
            int counter = 0;
            foreach (KeyValuePair<string, Tuple<double, string>> entry in data)
            {
                average += entry.Value.Item1;
                counter++;
            }
            average = average / counter;
            Console.WriteLine($"Your average is {average}");
        }
        public static void Methodthreefive(Dictionary<string, Tuple<double, string>> dacMarks)
        {
            var data = dacMarks;
            var aPlusList = new List<string>();
            foreach (KeyValuePair<string, Tuple<double, string>> entry in data)
            {
                if (entry.Value.Item1 >= 90)
                {
                    aPlusList.Add(entry.Key);
                }

            }
            foreach (var x in aPlusList)
            {
                Console.WriteLine($"{x} has passed with an A+");
            }
        }
        static void Dylans()
        {
            guessingGame();
        }

        static int numberOfGames = 0;
        static List<Tuple<int, int>> gamesPlayed = new List<Tuple<int, int>>();

        static void guessingGame()
        {
            numberOfGames += 1;
            var answer = 0;
            var score = 0;
            {
                Console.WriteLine("I'm thinking of a number between 1-5. You have 5 attempts to guess the correct numbers.\nEnter in a number:");
                for (int i = 0; i <= 4; i++)
                {

                    Random r = new Random();
                    answer = (r.Next(1, 5));

                    var guess = Convert.ToInt32(Console.ReadLine());
                    if (guess == answer)
                    {
                        Console.WriteLine("Correct.");
                        score += 1;
                    }
                    else if (guess != answer)
                    {
                        Console.WriteLine("Try again.", answer);
                    }

                }
                Console.WriteLine($"\nYou guessed {score} out of 5 correct.");

                gamesPlayed.Add(Tuple.Create(numberOfGames, score));

            }
            replay(score);
        }

        static void replay(int score)
        {
            Console.WriteLine("\nWould you like to play again? (y/n)");
            var a = Console.ReadLine();
            if (a == "y")
            {
                Console.Clear();
                guessingGame();
            }
            else if (a == "n")
            {
                Console.Clear();
                Console.WriteLine("Here is a list of your previous scores:\n");
                finalScores();

                Console.WriteLine("\nThankyou. See you next time.");
                Console.WriteLine("Press <enter> to return to the Main Menu");
                Console.ReadLine();
                Console.Clear();
            }
        }
        static void finalScores()
        {

            foreach (var x in gamesPlayed)
            {
                Console.WriteLine($"Game {x.Item1} - you scored {x.Item2}/5");
            }
        }
        static void FoodRank()
        {
            Ranking();
            Console.Clear();
            Lengthcheck(Ranking());

            Console.WriteLine("Please type in print to print your foods from 1 to 5");
            Console.WriteLine("Please type in change to change food you have entered into the list");

            var choice = 0;
            var select = Console.ReadLine();
            select = select.ToLower();
            while (choice != 1)
            {
                switch (select)
                {
                    case "print":

                        Printorder(Ranking());

                        Console.WriteLine("Type in change to change foods in list");
                        Console.WriteLine("Type in exit to exit");
                        select = Console.ReadLine();
                        select = select.ToLower();

                        break;

                    case "change":
                        Console.Clear();
                        ChangeFood(Ranking());
                        Console.WriteLine("Type in print to print foods in list");
                        Console.WriteLine("Type in exit to exit");
                        select = Console.ReadLine();
                        select = select.ToLower();
                        break;

                    case "exit":
                        choice = 1;
                        Console.Clear();
                        break;
                    default:
                        Console.WriteLine("Please enter change or print");
                        select = Console.ReadLine();
                        break;
                }
            }


        }
        static Dictionary<int, string> rank = new Dictionary<int, string>();
        static Dictionary<int, string> Ranking()
        {
            var i = 0;
            var count = 5;

            if (rank.Count == 0)
            {
                Console.WriteLine("Please enter your 5 favourite foods. Enter the rank and then food eg. 1<enter> Pizza<enter>");
                for (i = 0; i < count; i++)
                {
                    rank.Add(int.Parse(Console.ReadLine()), Console.ReadLine());
                }
            }
            return rank;
        }
        static Dictionary<int, string> Lengthcheck(Dictionary<int, string> rank)
        {
            if (rank.Count != 5)
            {
                rank = Ranking();
            }
            return rank;
        }
        static void Printorder(Dictionary<int, string> rank)
        {
            Console.Clear();
            var value = new List<string>();
            for (int i = 0; i <= rank.Count; i++)
            {
                if (rank.ContainsKey(i))
                {
                    value.Add(rank[i]);
                }
            }
            foreach (var x in value)
            {
                Console.WriteLine(x);
            }
        }
        static Dictionary<int, string> ChangeFood(Dictionary<int, string> rank)
        {
            foreach (var x in rank)
            {
                Console.WriteLine($"{x.Key}, {x.Value}");
            }
            Console.WriteLine($"Please enter change to change food in the list.");
            var choice = 0;
            var select = Console.ReadLine();
            select = select.ToLower();
            while (choice != 1)
            {
                switch (select)
                {
                    case "change":
                        do
                        {
                            Console.WriteLine("Please enter a number to delete that entry");
                            rank.Remove(int.Parse(Console.ReadLine()));
                            Console.WriteLine("Please enter a food into the rank removed eg 1<enter> Pizza<enter>");
                            rank.Add(int.Parse(Console.ReadLine()), Console.ReadLine());
                            Console.WriteLine("Enter change to change another ranking or enter exit");
                            select = Console.ReadLine();
                            select = select.ToLower();
                        }
                        while (select == "change");
                        break;
                    case "exit":
                        choice = 1;

                        break;
                    default:
                        Console.WriteLine("Please enter add or delete");
                        select = Console.ReadLine();
                        break;
                }
            }

            return rank;
        }
    }
}























































